const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const requireDir = require('require-dir')
const dotenv = require('dotenv')

// Checking enviroment  
if(process.env.NODE_ENV !== 'production') {
  dotenv.config()
}
// Server init
const app = express()
app.use(express.json())
app.use(cors())

// Database init
mongoose.connect('mongodb://localhost:27017/test-tauria', { useNewUrlParser: true, useUnifiedTopology: true })

requireDir('./src/models')

// Routes init
app.use('/api', require('./src/routes'))

app.listen(3001)