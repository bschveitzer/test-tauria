## Test project for Tauria

### Tools

1. NodeJs 12.14.1
2. Yarn or NPM

### Init commands

1. *yarn* or *npm install* install all dependencies
2. *yarn run dev* initialize server in local enviroment [localhost:8080](https://localhost)

### API documentation

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/ad5450778f6461e378e3)

