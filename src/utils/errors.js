exports.loginErrors = {
  NotFound: {
    message: 'Wrong username or password.'
  }
}

exports.crudUserErrors = {
  ValidationError: {
    message: 'Missing user info.'
  },
  MongoError: {
    message: 'User already register in the database.',
    action: 'recoverPassword'
  }
}

exports.crudRoomErrors = {
  DuplicatedUser: {
    message: 'You are already in this call.'
  }
}