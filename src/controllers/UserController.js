const mongoose = require('mongoose')
const User = mongoose.model('User')

module.exports = {
  updateUser: async (req, res) => {
    try {
      const { password, mobileToken } = req.body
      const user = await User.findByIdAndUpdate(req.user, { password, mobileToken }, { new: true })
      return res.json(user)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  removeUser: async (req, res) => {
    try {
      const user = await User.findByIdAndUpdate(req.user, { active: false }, { new: true })
      return res.json(user)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  getUserRooms: async (req, res) => {
    try {
      const rooms = await User.findOne({ login: req.params.login }, 'rooms')
        .populate({ 
         path: 'rooms'
      })

      return res.json(rooms)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
}