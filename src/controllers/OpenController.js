const mongoose = require('mongoose')
const User = mongoose.model('User')
const Room = mongoose.model('Room')
const jwt = require('jsonwebtoken')
const error = require('../utils/errors')

module.exports = {
  login: async (req, res) => {
    try {
      const userInfo = await User.findOne(req.body, '-password -createdAt -updatedAt')

      if(!userInfo) return res.status(400).json(error.loginErrors['NotFound'])

      jwt.sign({ userInfo }, 'tauriaKey', (err, token) => {
        res.json({ userInfo, token })
      })

    } catch (err) {
      return res.status(400).json(err)
    }
  },

  createUser: async (req, res) => {
    try {
      const user = await User.create(req.body) 
      return res.json(user)
    } catch (err) {
      const responseErr = error.crudUserErrors[err.name] ? error.crudUserErrors[err.name] : err
      return res.status(400).json(responseErr)
    } 
  },

  getAllUsers: async (req, res) => {
    const { page = 1, limit = 10 } = req.query
    const users = await User.paginate({}, { page, limit })
    return res.json(users)
  },

  getUserInfo: async (req, res) => {
    const user = await User.findOne({ login: req.params.login })
    return res.json(user)
  },

  getRoomInfo: async (req, res) => {
    const room = await Room.findById(req.params.id)
    return res.json(room)
  }
}