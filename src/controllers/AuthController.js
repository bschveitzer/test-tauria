const jwt = require('jsonwebtoken')

module.exports = {
  verifyAuth: async (req, res, next) => {
    const bearerHeader = req.headers['authorization']

    if(!bearerHeader) return res.sendStatus(403)

    const bearer = bearerHeader.split(' ')

    const bearerToken = bearer[1]

    jwt.verify(bearerToken, 'tauriaKey', (err, authData) => {
      if(err) return res.sendStatus(403)

      if(req.path === '/user/me') return res.json(authData)

      req.user = authData.userInfo._id
      
      next()
    })

  }
}