const mongoose = require('mongoose')
const Room = mongoose.model('Room')
const User = mongoose.model('User')
const error = require('../utils/errors')

module.exports = {
  createRoom: async (req, res) => {
    try {
      const newRoom = { ...req.body, members: [req.user]}
      const room = await Room.create(newRoom) 
      return res.json(room)
    } catch (err) {
      return res.status(400).json(err)
    } 
  },
  changeHost: async (req, res) => {
    try {
      const { hostUser } = req.params.body
      const room = await Room.findByIdAndUpdate(req.params.id, { hostUser }, { new: true })
      return res.json(room)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  joinRoom: async (req, res) => {
    try {
      const verifyUser = await Room.findOne({_id: req.params.id, members: req.user })

      if(verifyUser) return res.status(400).json(error.crudRoomErrors['DuplicatedUser'])

      const room = await Room.findByIdAndUpdate(req.params.id, { $push: { members: req.user }}, { new: true })

      await User.findByIdAndUpdate(req.user, { $push: { rooms: req.params.id }})
      
      return res.json(room)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  leaveRoom: async (req, res) => {
    try {
      await Room.findByIdAndUpdate(req.params.id, { $pull: { members: req.user }}, { new: true })
      await User.findByIdAndUpdate(req.user, { $pull: { rooms: req.params.id }})
      return res.json({ success: true})
    } catch (err) {
      return res.status(400).json(err)
    }
  }
}