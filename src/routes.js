const express = require('express')
const routes = express.Router()

const UserController = require('./controllers/UserController')
const OpenController = require('./controllers/OpenController')
const AuthController = require('./controllers/AuthController')
const RoomController = require('./controllers/RoomController')

// Open routes
routes.get('/users', OpenController.getAllUsers)
routes.get('/user/:login', OpenController.getUserInfo)
routes.get('/room/:id', OpenController.getRoomInfo)
routes.post('/login', OpenController.login)
routes.post('/user', OpenController.createUser)

// Auth controlled routes
routes.get('/user/me', AuthController.verifyAuth)
routes.get('/userRooms/:login', AuthController.verifyAuth, UserController.getUserRooms)
routes.put('/user/:id', AuthController.verifyAuth, UserController.updateUser)
routes.put('/removeUser/:id', AuthController.verifyAuth, UserController.removeUser)
routes.post('/room', AuthController.verifyAuth, RoomController.createRoom)
routes.put('/roomHost', AuthController.verifyAuth, RoomController.changeHost)
routes.put('/join/:id', AuthController.verifyAuth, RoomController.joinRoom)
routes.put('/leave/:id', AuthController.verifyAuth, RoomController.leaveRoom)

module.exports = routes