const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const RoomSchema = new mongoose.Schema({
	name: {
		type: String,
		trim: true,
		required: true,
	},
	hostUser: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User'
  },
	members: [{
		type: mongoose.Schema.Types.ObjectId, 
    ref: 'User'
  }],
	limit: {
    type: Number,
    default: 5
  },
	active: {
		type: Boolean,
		default: true
	}
}, { toJSON: { virtuals: true }, timestamps: true })

RoomSchema.plugin(mongoosePaginate)

mongoose.model('Room', RoomSchema)