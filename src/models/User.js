const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const UserSchema = new mongoose.Schema({
	login: {
		type: String,
		trim: true,
		required: true,
		unique: true
	},
	password: {
		type: String,
		trim: true,
		required: true
	},
	name: {
		first: {
			type: String,
			trim: true,
			required: true
		},
		last: {
			type: String,
			trim: true,
			required: true
		}
	},
	mobileToken: {
		type: String,
		match: /^[0-9]*$/,
		minlength: 6,
		maxlength: 6
	},
	rooms: [{
		type: mongoose.Schema.Types.ObjectId, 
    ref: 'Room'
  }],
	active: {
		type: Boolean,
		default: true,
		required: true
	}
}, { toJSON: { virtuals: true }, timestamps: true })

UserSchema.plugin(mongoosePaginate)

UserSchema.virtual('fullName').get(function () {
  return `${this.name.first} ${this.name.last}`;
});

mongoose.model('User', UserSchema)